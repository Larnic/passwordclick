
/*
* Formulaire de connéxion sans taper son mot de passe
* au clavier. Pour des reaispons de sécurité.
*
* Pour plus de confort la connéxion peut se faire via AJAX et 
* rediriger en cas de succès.
* Ce la évite la demande de sauvegarde des navigateurs.
*/

var liste = document.querySelectorAll('li');
var total = liste.length;
var password = document.querySelector('input[type=password]');
var effacer = document.querySelector("input[type='button']:nth-of-type(3)");

/*
* Nombres à générer
*/

var nombres = [1,2,3,4,5,6,7,8,9];

/*
* Position dans la grille à l'écran
*/
var positions = [0,1,2,3,4,5,6,7,8,9,10,11];


 while(nombres.length > 0){
 	var nombre = generate();
 	var position = getPosition();

 	liste[position].querySelector('p').textContent = nombre;
 }

/*
* Sort un nombre aléatoire dans la liste nombres
*/
function generate(){

	var vretour = 0;

	if(nombres.length > 0){

  		vretour = nombres[Math.floor(Math.random()*nombres.length)];
  		
  		nombres.splice(nombres.indexOf(vretour),1);
	}

	return vretour;
}

/*
* Sort un nombre dans la liste positions
*/
function getPosition(){

	var vretour = 0;

	if(positions.length > 0){

  		vretour = positions[Math.floor(Math.random()*positions.length)];
  		
  		positions.splice(positions.indexOf(vretour),1);
	}

	return vretour;
}


/*
* Ajout des listeners sur les li p
*/

liste.forEach(function(valeur, index){
	valeur.addEventListener("click", function() {
		if(valeur.textContent != "" && password.value.length < 7){
			effacer.disabled =  false;
			password.value = password.value+valeur.textContent;
		}
	});
});

/*
* Supprimer un caractères
*/
effacer.addEventListener("click", function() {
	if(password.value.length > 0){
		password.value = password.value.substring(password.value.length, 1);

		if(password.value.length == 0){
			effacer.disabled =  true;
		}
	}
});